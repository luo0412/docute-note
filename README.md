# docute文档

  - https://docute.js.org/#/home

```javascript
cnpm i -g docute-cli

docute init ./docs

docute ./docs

更多这个命令的参数可以执行 `docute init --help` 来查看

<img src="/assets/command-preview.png" alt="preview" width="500">
你可以使用 `--watch` 来开启 live reload 功能，更多这个命令的参数可以执行 `docute --help` 来查看。
```

# hexo博客






