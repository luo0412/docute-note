docute.init({
    tocVisibleDepth: 3, //
    nav: [
        {title: '手册说明', path: '/'},
        {title: '编程规范', path: '/ch0-standard'},
        {title: '布局', path: '/ch1-layout'},
        {title: '表单/数据交互', path: '/ch2-form'},
        {title: '动画/装饰', path: '/ch3-effects'},
        {title: '工具', path: '/ch4-tool'},
        {title: '小游戏', path: '/ch5-game'},
        {title: 'Web-VR', path: '/ch6-vr'}
    ]
});